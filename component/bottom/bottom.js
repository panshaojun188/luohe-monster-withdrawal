// components/drawer/index.js
Component({
  options: {
    styleIsolation: 'apply-shared'
  },
  /**
   * 组件的属性列表
   */
  properties: {
    title: {
      type: String,
      value: ''
    },
    confirmBtn:{
      type: Boolean,
      value: false
    },
    cencelBtn: {
      type: Boolean,
      value: true
    },
    closeBtn:{
      type: Boolean,
      value: false
    },
    show: {
      type: Boolean,
      value: false
    },
    maskClose: {
      type: Boolean,
      value: true
    },
    modalType: {
      type: String,
      value: ''
    }
  },
  /**
   * 组件的初始数据
   */
  data: {
    text: ''
  },
  ready: function () {
  },
  /**
   * 组件的方法列表
   */
  methods: {
    eventClose () {
      if (this.data.maskClose) {
        this.close()
      }
    },
	loginout() {
    wx.showModal({
      title: '提示',
      content: '您确定要退出登录吗',
      success: function (res) {
        if (res.confirm) { //这里是点击了确定以后
          console.log('用户点击确定')
          wx.setStorageSync('token', ''); //将token置空
          wx.redirectTo({
            url: '/pages/login/index', //跳去登录页
          })
        } else { //这里是点击了取消以后
          console.log('用户点击取消')
        }
      }
    })
  },
    close() {
      this.setData({
        show: false
      });
      this.triggerEvent('close');
    },
    confirm(){
      this.triggerEvent('confirm');
    }
  }
})
