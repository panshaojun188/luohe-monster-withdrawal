const app = getApp();

/**
 * 选择图片路径
 */
function chooseImageTap() {
  return new Promise((resolve, reject) => {
    wx.showActionSheet({
      itemList: ['从相册中选择', '拍照'],
      itemColor: "#00000",
      success(res) {
        if (!res.cancel) {
          let type = res.tapIndex == 0 ? 'album' : 'camera';
          chooseWxImage(type).then(res => {
            resolve(res);
          })
        }
      }
    })
  });
}

/**
 * 选择图片
 */
function chooseWxImage(type) {
  return new Promise((resolve, reject) => {
    wx.chooseImage({
      sizeType: ['original', 'compressed'],
      sourceType: [type],
      success(res) {
        let name = res.tempFilePaths[0];
        name.replace("http://tmp/", "");
        wx.showLoading({
          title: '上传中',
        });
        uploadFile({
          file: res.tempFilePaths[0],
          name: name
        }).then(url => {
          resolve({
            url: url,
            type: 1
          });
        });
      },
      fail(err) {
        if(err.errMsg != 'chooseImage:fail cancel'){
          wx.showToast({
            title: err.errMsg,
            icon: 'none',
            duration: 2500
          })
        }        
      }
    })
  });
}

/**
 * 上传文件
 */
function uploadFile(query) {
  const that = this, app = getApp(),
    formData = {};
  formData.file = query.file;
  formData.token = wx.getStorageSync('token');
  return new Promise((resolve, reject) => {
    wx.uploadFile({
      url: `${app.globalData.requestUrl}Image/uploadImage`,
      filePath: query.file,
      name: 'file',
      formData: formData,
      header: {
        "Content-Type": "multipart/form-data",
        'accept': 'application/json',
      },
      success(res) {
        console.log(res);
        wx.hideLoading();
        if (res.statusCode == 200) {
          wx.showToast({
            title: '上传成功'
          });
          let info = JSON.parse(res.data);
          console.log(info);
          wx.showToast({
            title: '上传成功' + info.data.file_url,
          });
          resolve(info.data.file_url);
        } else {
          wx.showToast({
            title: res.errMsg,
            icon: 'none'
          });
        }
      },
      fail(res) {
        wx.hideLoading();
        wx.showToast({
          title: '网络连接错误',
          icon: 'none'
        });
      }
    })
  })
}

function pay(json) {
  return new Promise((resolve, reject) => {
    wx.requestPayment({
      appId: json.appid,
      timeStamp: json.timeStamp + "",
      nonceStr: json.nonceStr,
      package: json.package,
      signType: json.signType,
      paySign: json.paySign,
      success(res) {
        resolve(res)
      },
      fail(res) {
        reject(res);
      },
      complete(res) {
        console.log(res)
      }
    })
  })
}

function getLocation(json = {}) {
  return new Promise((resolve, reject) => {
    wx.getLocation({
      type: json.type || 'gcj02',
      success(res) {
        resolve({
          code: 0,
          data: res
        })
      },
      fail(err) {
        console.log('locationfail')
        console.log(err)
        if (err.error == 2001) {
          wx.getSetting({
            success: (res) => {
              if (res.authSetting['scope.userLocation'] != undefined && res.authSetting['scope.userLocation'] != true) { //非初始化进入该页面,且未授权
                wx.confirm({
                  title: '是否授权当前位置',
                  content: '需要获取您的地理位置，请确认授权',
                  success(res) {
                    if (res.cancel) {
                      wx.showToast({
                        content: '取消授权'
                      });
                      reject()
                    } else if (res.confirm) {
                      wx.openSetting({
                        success(dataAu) {
                          if (dataAu.authSetting["scope.location"] == true) {
                            getLocation().then(rres => {
                              resolve({
                                code: 0,
                                data: rres
                              })
                            })
                          } else {
                            wx.showToast({
                              title: '定位失败'
                            })
                            reject()
                          }
                        },
                        fail() {
                          reject()
                        }
                      })
                    }
                  },
                  fail(err) {
                    reject(err)
                  }
                })
              }
            }
          })
        } else {
          wx.showModal({
            content: err.errorMessage,
            showCancel: false,
            success(res) {
              if (res.confirm) {

              }
            }
          })
          reject()
        }
      }
    })
  })
}

function isTerminal(level) {
  level = level || wx.getStorageSync('agent_level')
  return level == 0
}

function isPartner(level) {
  level = level || wx.getStorageSync('agent_level')
  return level == 1
}

function isAgent(level) {
  level = level || wx.getStorageSync('agent_level')
  return level == 2
}

function isDirector(level) {
  level = level || wx.getStorageSync('agent_level')
  return level == 3
}

function isSalesman(level) {
  level = level || wx.getStorageSync('agent_level')
  return level == 4
}

function isMerchants(level) {
  level = level || wx.getStorageSync('agent_level')
  return level == 5
}

function templateMsg(tmplIds) {
  return new Promise((resolve, reject) => {
    wx.requestSubscribeMessage({
      tmplIds: tmplIds,
      success(res) {
        console.log(res)
        console.log('templateMsg -- success')
        console.log(res)
        if (res[tmplIds[0]] == 'accept') {
          resolve(0)
        } else if (res[tmplIds[0]] == 'ban') {
          wx.showModal({
            title: '很抱歉',
            content: `订阅消息已被禁用(${res[tmplIds[0]]})`,
            showCancel: false,
            success(res) {
              reject()
            }
          })
        } else {
          reject()
        }
      },
      fail(err) {
        console.log('templateMsg -- fail')
        console.log(err)
        wx.showModal({
          title: '很抱歉',
          content: '订阅消息失败',
          showCancel: false,
          success(res) {
            reject()
          }
        })
      }
    })
  })
}

/**
 * Parse the time to string
 * @param {(Object|string|number)} time
 * @param {string} cFormat
 * @returns {string | null}
 */
function parseTime(time, cFormat) {
  if (arguments.length === 0) {
    return null
  } else if (!time) {
    return '--'
  }

  const format = cFormat || '{y}-{m}-{d} {h}:{i}:{s}'
  let date
  if (typeof time === 'object') {
    date = time
  } else {
    if ((typeof time === 'string') && (/^[0-9]+$/.test(time))) {
      time = parseInt(time)
    }
    if ((typeof time === 'number') && (time.toString().length === 10)) {
      time = time * 1000
    }
    date = new Date(time)
  }
  const formatObj = {
    y: date.getFullYear(),
    m: date.getMonth() + 1,
    d: date.getDate(),
    h: date.getHours(),
    i: date.getMinutes(),
    s: date.getSeconds(),
    a: date.getDay()
  }
  const time_str = format.replace(/{([ymdhisa])+}/g, (result, key) => {
    const value = formatObj[key]
    // Note: getDay() returns 0 on Sunday
    if (key === 'a') {
      return ['日', '一', '二', '三', '四', '五', '六'][value]
    }
    return value.toString().padStart(2, '0')
  })
  return time_str
}

/**
 * 日期转时间戳
 * @param  datetime
 * @return {[unix]}
 */
function unixTime(datetime) {
  var tmp_datetime = datetime.replace(/:/g, '-');
  tmp_datetime = tmp_datetime.replace(/ /g, '-');
  var arr = tmp_datetime.split("-");
  var now = new Date(Date.UTC(arr[0], arr[1] - 1, arr[2], arr[3] ? arr[3] - 8 : '00', arr[4] ? arr[4] : '00'));
  return parseInt(now.getTime() / 1000);
}

/**
 * 千分位逗号去除
 */
function delComma(num) {
  if(typeof num === 'number' || !isNaN(num)){
    return num
  }
  return num.replace(/,/g, '');
}

/**
 * 计算百分比
 */
function countPer(num, total) {
  num = parseFloat(num.toString().replace(/[^\d\.-]/g, ''))
  if (typeof total === 'string') {
    total = (total.trim()).toString()
    if (total.length == 0) {
      total = 0
    } else {
      total = delComma(total)
    }
  }
  if (isNaN(num) || isNaN(total)) return 0
  return total <= 0 ? 0 : (Math.round(num / total * 10000) / 100.00)
}

/**
 * 设备类型
 * type 1 返回所有设备类型 2 返回设备名称 3 返回设备英文key
 */
function frontGoodsType(get = 1, goods_type = 0) {
  const app = getApp()
  if (!app.globalData.siteInfo) return
  let device_type = app.globalData.siteInfo.device_type, goods = {}, goodsKey = {}, allKey = {
    0: '充电宝',
    4: '洗衣机',
    1: '密码线',
    2: '按摩枕',
    3: '充电桩',
    6: '电吹风',
    7: '套套机',
    8: '加湿器'
  }
  for (var i in device_type) {
    let d = device_type[i]
    goods[d.depend_name] = d.depend_type
    goodsKey[d.depend_type] = d.depend_name
  }  
  if (get == 1) {
    return goods
  } else if (get == 2) {
    return allKey[goods_type]
  } else if (get == 4) {
    return allKey
  } else if (get == 3) {
    return goodsKey
  }
}

/**
 * 设备类型
 * type 1 返回所有设备类型 2 返回设备名称 3 返回设备英文key
 */
function goodsType(get = 1, device_type = 0) {
  const app = getApp()
  if (app.globalData.deviceNameObj) {
    let deviceNameObj = app.globalData.deviceNameObj,
      deviceKeyObj = app.globalData.deviceKeyObj
    if (get == 1) {
      return deviceNameObj
    } else if (get == 2) {
      return deviceKeyObj[device_type]
    } else if (get == 4) {
      return deviceKeyObj
    }
  } else {
    app.agentGet('agentapi/ucenter/my_device_type').then(res => {
      let deviceNameObj = {}, deviceKeyObj = {}
      for (var i in res) {
        let d = res[i]
        if (d.taked > 0) {
          deviceNameObj[d.depend_name] = d.depend_type
          deviceKeyObj[d.depend_type] = d.depend_name
        }
      }
      app.globalData.deviceNameObj = deviceNameObj
      app.globalData.deviceKeyObj = deviceKeyObj
      app.globalData.agentDevice = res
      app.getPage().onLoad(app.getPage().options)
    })
  }
}

/**
 * 设备类型
 * type 1 返回设备类型icon
 */
function goodsIcon(goods_type = '') {
  let icon = {
    0: 'icon-chongdianbao',
    4: 'icon-xiyiji',
    1: 'icon-icon-chongdianxian',
    2: 'icon-zhentou',
    3: 'icon-chongdianzhuang',
    6: 'icon-meizhuangpian-icon-'
  }
  return (goods_type ? icon[goods_type] : icon)
}

/**
 * 图表设备颜色值
 */
function colorPalette() {
  return {
    0: '#2ec7c9',
    4: '#b6a2de',
    2: '#5ab1ef',
    3: '#ffb980',
    1: '#d87a80',
    5: '#8d98b3',
    6: '#e5cf0d',
    7: '#97b552',
    8: '#95706d',
    // '#dc69aa',
    // '#07a2a4',
    // '#9a7fd1',
    // '#588dd5',
    // '#f5994e',
    // '#c05050',
    // '#59678c',
    // '#c9ab00',
    // '#7eb00a',
    // '#6f5553',
    // '#c14089'
  }
}

function currentTime () {
  return Date.parse(new Date()) / 1000;
}

const countdownObj = {}

/**
 * 倒计时
 */
function countdown(timeArr, cbOk, intervalName) {
  if (!timeArr) return '';
  intervalName = intervalName || 'interval';
  countdownObj[intervalName] = setInterval(function () {
    var returnArr = [], second = '', timeStatus, current_time = parseInt(currentTime());
    for (var i in timeArr) {
      var start_time = parseInt(timeArr[i].start_time), end_time = parseInt(timeArr[i].end_time);
      if (start_time > current_time) {
        second = start_time - current_time;
        timeStatus = 1
      } else if (end_time > current_time) {
        second = end_time - current_time;
        timeStatus = 2
      } else {
        returnArr.push({
          day: 0,
          hour: 0,
          minute: 0,
          second: 0,
          status: -1
        })
        continue
      }

      // 天数位
      var day = Math.floor(second / 3600 / 24);
      var dayStr = day.toString();
      if (dayStr.length == 1) dayStr = '0' + dayStr;

      // 小时位
      var hr = Math.floor((second - day * 3600 * 24) / 3600);
      var hrStr = hr.toString();
      if (hrStr.length == 1) hrStr = '0' + hrStr;

      // 分钟位
      var min = Math.floor((second - day * 3600 * 24 - hr * 3600) / 60);
      var minStr = min.toString();
      if (minStr.length == 1) minStr = '0' + minStr;

      // 秒位
      var sec = second - day * 3600 * 24 - hr * 3600 - min * 60;
      var secStr = sec.toString();
      if (secStr.length == 1) secStr = '0' + secStr;

      returnArr.push({
        day: dayStr,
        hour: hrStr,
        minute: minStr,
        second: secStr,
        status: timeStatus
      })
    }
    cbOk && cbOk(returnArr)
  }, 1000);
}

function clearIvl(name) {
  clearInterval(countdownObj[name])
}

/**
 * 浮点数加法
 */
function accAdd(){
  var args = arguments, lens = args.length, d = 0, sum = 0;
  
  for (var key in args) {
    var str = "" + args[key];
    if (str.indexOf(".") != -1) {
      var temp = str.split(".")[1].length;
      d = d < temp ? temp : d;
    }
  }

  //计算需要乘的数值
  var m = Math.pow(10, d);
  
  //遍历所有参数并相加
  for (var key in args) {
    sum += args[key] * m;
  }

  return sum / m;
}

/**
 ** 浮点数减法
 **/
function accSub(arg1, arg2) {
  var args = arguments, lens = args.length, d = 0, bnum = 0, sum = 0;

  for (var key in args) {
    var str = "" + args[key];
    if (str.indexOf(".") != -1) {
      var temp = str.split(".")[1].length;
      d = d < temp ? temp : d;
    }
  }

  //计算需要乘的数值
  var m = Math.pow(10, d);

  //遍历所有参数并相减
  for (var key in args) {
    if(key > 0){
      bnum = bnum || args[key - 1] * m;
      sum = bnum - (args[key] * m);
    }
  }
  return sum / m;
}

/**
 ** 计算剩余时间
 **/
function formatSeconds(value) {
  var theTime = parseInt(value)// 秒
    var middle = 0// 分
    var hour = 0// 小时
    if (theTime > 60) {
      middle = parseInt(theTime / 60)
      theTime = parseInt(theTime % 60)
      if (middle > 60) {
        hour = parseInt(middle / 60)
        middle = parseInt(middle % 60)
      }
    }
    var result = '' + parseInt(theTime) + '秒'
    if (middle > 0) {
      result = '' + parseInt(middle) + '分' + result
    }
    if (hour > 0) {
      result = '' + parseInt(hour) + '时' + result
    }
    return result
}

/**
 * 设置对象值
 */
function getSetValueObj(e){
  let dataset = e.currentTarget.dataset, v = e.detail.value
  return {
    [`${dataset.obj ? dataset.obj + '.' : ''}${dataset.name}`]: v,
  }
}

/**
 * 获取模拟数据
 */
function getSimulateData(type, id = ''){
  let key = 'simulate_data'
  if(id > 0) key = key + '_' + id
  let simulateData = wx.getStorageSync(key)
  if(simulateData.simulate_switch){
    return simulateData[type]
  }
  return {}
}

/**
 * 提取二维数组指定key
 */
function arrayKeys(array = [], key = '', nkey = ''){
  if(!key) return nkey ? {} : []
  let arr = nkey ? {} : []
  array.map(item => {
    if(nkey) arr[nkey] = item[key]
    if(!nkey) arr.push(item[key])
  })
  return arr
}

module.exports = {
  pay: pay,
  chooseImageTap: chooseImageTap,
  chooseWxImage: chooseWxImage,
  getLocation: getLocation,
  isTerminal: isTerminal,
  isPartner: isPartner,
  isAgent: isAgent,
  isDirector: isDirector,
  isSalesman: isSalesman,
  isMerchants: isMerchants,
  templateMsg: templateMsg,
  parseTime: parseTime,
  unixTime: unixTime,
  delComma: delComma,
  countPer: countPer,
  goodsType: goodsType,
  frontGoodsType: frontGoodsType,
  colorPalette: colorPalette,
  goodsIcon: goodsIcon,
  countdown: countdown,
  currentTime: currentTime,
  clearIvl: clearIvl,
  accAdd: accAdd,
  accSub: accSub,
  formatSeconds: formatSeconds,
  getSetValueObj: getSetValueObj,
  getSimulateData: getSimulateData,
  arrayKeys: arrayKeys
}