const app = getApp();
Component({
  /**
   * 组件的一些选项
   */
  options: {
    addGlobalClass: true,
    multipleSlots: true
  },
  /**
   * 组件的对外属性
   */
  properties: {
    bgColor: {
      type: String,
      default: ''
    }, 
    isCustom: {
      type: [Boolean, String],
      default: false
    },
    isBack: {
      type: [Boolean, String],
      default: false
    },
    bgImage: {
      type: String,
      default: ''
    },
    avatar: {
      type: String,
      default: ''
    },
    avatarUrl: {
      type: String,
      default: ''
    },    
  },
  /**
   * 组件的初始数据
   */
  data: {
    StatusBar: app.globalData.StatusBar,
    CustomBar: app.globalData.CustomBar || 84,
    Custom: app.globalData.Custom
  },

  ready(){
    console.log(this.data.StatusBar, this.data.CustomBar)
    if(!this.data.StatusBar){
      let Custom = {
        width: 87,
        height: 32,
        right: 365
      }, setObj
      if(app.globalData.StatusBar > 0){
        setObj = {
          StatusBar: app.globalData.StatusBar,
          CustomBar: app.globalData.CustomBar,
          Custom: app.globalData.Custom
        }
        this.setData(setObj)
        app.getPage().setData(setObj)
      } else {
        wx.getSystemInfo({
          success: e => {
            let StatusBar = e.statusBarHeight || 44, CustomBar
            let capsule = wx.getMenuButtonBoundingClientRect()
            if (capsule) {
              Custom = capsule
              CustomBar = capsule.bottom + capsule.top - e.statusBarHeight
            } else {
              CustomBar = e.statusBarHeight + 50
            }
            setObj = {
              StatusBar: StatusBar,
              CustomBar: CustomBar || 84,
              Custom: Custom
            }
            this.setData(setObj)
            app.getPage().setData(setObj)
          },
          fail: e => {
            setObj = {
              StatusBar: 44,
              CustomBar: 84,
              Custom: Custom
            }
            this.setData(setObj)
            app.getPage().setData(setObj)
          }
        })
      }
    }
  },

  /**
   * 组件的方法列表
   */
  methods: {
    BackPage() {
      wx.navigateBack({
        delta: 1
      });
    },
    toHome(){
      wx.reLaunch({
        url: '/pages/agent/index/index',
      })
    }
  }
})