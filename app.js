/**
 * 充电小程序
 * aaa
 * 主要实现功能，用户登录授权校验，用户后台数据存储，用户后台注册功能
 */
import {
  isMerchants,
  isSalesman,
  isTerminal,
  parseTime
} from './utils/util.js'
App({
  onLaunch(options) {
    wx.getSystemInfo({
      success: e => {
        this.globalData.StatusBar = e.statusBarHeight
        let capsule = wx.getMenuButtonBoundingClientRect()
        if (capsule) {
          this.globalData.Custom = capsule
          this.globalData.CustomBar = capsule.bottom + capsule.top - e.statusBarHeight
        } else {
          this.globalData.CustomBar = e.statusBarHeight + 50
        }
      }
    })
    let extConfig = wx.getExtConfigSync ? wx.getExtConfigSync() : {}
    this.globalData.app_id = extConfig.app_id || wx.getAccountInfoSync().miniProgram.appId
    this.globalData.agent_id = extConfig.agent_id || 96
    this.globalData.systemInfo = wx.getSystemInfoSync()
    
    this.getSiteInfo()
  },
  globalData: {
    app_id: '', //小程序APP_ID
    opendi: null,
    smobile: null,
    template_ids: {},
    siteShare: {},
    cardObj: [{
        key: 'month',
        name: '月卡',
        value: 0
      },
      {
        key: 'season',
        name: '季卡',
        value: 1
      },
      {
        key: 'half_year',
        name: '半年卡',
        value: 2
      },
      {
        key: 'year',
        name: '年卡',
        value: 3
      }
    ],
    percent: {
      0: 'bao',
      1: 'thread',
      3: 'zhuang'
    },
    profit_key: {
      0: '',
      1: 'thread_',
      3: 'zhuang_'
    },
    pay_way: {
      depositFree: -1,
      deposit: 1,
      card: 4,
      balance: 3,
      online: 2,
      clubcard: 5,
    },
    pay_way_name: {
      4: '充值卡',
      3: '钱包',
      2: '微信支付'
    },
    withdraw_way: ['银行卡', '微信提现', '支付宝提现', '微信收款码', '支付宝收款码'],
    thread_time: [0.5, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12],
    day_unit: [5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 22, 24],
    condom_time: [0.5, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12],
    humidifier_time: [0.5, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12],
    blower_time: [3, 5, 8, 10],
    zhuang_unit: [0.5, 1],
    zhuang_power_max: 99999,
    ossUrl: 'https://xiaomeicdb.oss-cn-hangzhou.aliyuncs.com/resources/static/', // oss域名 + resources/static/
    thread_mp3: '155963027.mp3',
    web_url: 'https://admin.cnscooter.cn/',
    requestUrl: "https://www.cnmonster.cn/",
    wpturl: "https://www.cnmonster.cn/",
    tabBar: {
      "backgroundColor": "#ffffff",
      "color": "#333",
      "selectedColor": "#333",
      "list": [

      ]
    }
  },
  /**
   * 小程序展示
   */
  onShow(options) {
    // this.hidetabbar()
    this.globalData.loginStatus = false
    // if (options.scene == 1038) {
    //   let {
    //     extraData
    //   } = options.referrerInfo, query_id = ''
    //   if (extraData && extraData.query_id) query_id = extraData.query_id
    //   this.queryOrderStatus(query_id)
    // }
  },

  /**
   * 登录状态重置
   */
  loginReset() {
    this.globalData.loginStatus = false
  },

  /**
   * 获取合伙人小程序平台信息
   */
  getSiteInfo() {


    let that = this,
      page = this.getPage(),
      json = {}
    return new Promise((resolve, reject) => {


      if (that.globalData.siteInfo) {
        if (page) {
          page.setData({
            siteInfo: that.globalData.siteInfo
          })
        }
        resolve(that.globalData.siteInfo)
      } else {


        /**/
        that.get('commonapi/get_platform_config').then(res => {


          //   res.time_unit = res.fund_fee_time_unit == 0 ? '分钟' : '小时'
          //  res.wx_adver_spacing = res.wx_ads_closing == 1
          that.globalData.siteInfo = res
          if (page) {
            page.setData({
              siteInfo: that.globalData.siteInfo
            })
          }
          resolve(res)
        })
      }
    })
  },


 

  /**
   * 获取用户信息
   * login 1 强制登录 2 不强制登录
   */
  getUserInfo(login = 1) {
    var that = this,
      page = getCurrentPages()[getCurrentPages().length - 1]
    return new Promise((resolve, reject) => {
      if (that.globalData.userInfo) {
        if (page) {
          page.setData({
            userInfo: that.globalData.userInfo
          })
        }
        resolve(that.globalData.userInfo);
      } else {
        that.get('commonapi/get_user_status', {
          login: login,
          user_type: 1
        }).then(res => {
          if (page) {
            page.setData({
              userInfo: res
            })
          }
          that.globalData.userInfo = res
          resolve(res)
        }).catch(res => {
          reject()
        })
      }
    })
  },

  /**
   * 提交操作记录
   */
  postSetRecode(tag_id) {
    let that = this
    that.agentPost('agentapi/log/add_record', {
      tag_id: tag_id
    }).then(res => {

    })
  },

  /**
   * 提交统计
   */
  postUsing(is_failed_stat = 1, val) {
    let that = this,
      params = {
        is_failed_stat: is_failed_stat
      }
    if (is_failed_stat == 1) params.order_id = val
    if (is_failed_stat == 2) params.sao_sn = val
    if (!params.sao_sn && !params.order_id) return
    that.post('wxapi/order/stop_thread_using_action', params).then(res => {

    })
  },

  /**
   * 获取小程序平台广告
   */
  getAdunit() {
    /* let that = this
    return new Promise((resolve, reject) => {
      if (that.globalData.adunit) {
        resolve(that.globalData.adunit)
      } else {
        that.get('commonapi/ad/get_wx_ad_ids').then(res => {
          that.globalData.adunit = res
          resolve(that.globalData.adunit)
        })
      }
    }) */
  },

  /**
   * 获取小程序平台模板消息
   */
  getTemplate(type = 1) {
    let that = this,
      page = this.getPage()
    return new Promise((resolve, reject) => {
      if (that.globalData.template_ids[type]) {
        if (page) {
          page.setData({
            template_ids: that.globalData.template_ids[type]
          })
        }
        resolve(that.globalData.template_ids[type])
      } else {
        that.get('commonapi/configer/get_platform_news_mould_keys', {
          mould_type: type
        }).then(res => {
          let template_ids = []
          for (var i in res) {
            template_ids.push(res[i].template_id)
          }
          that.globalData.template_ids[type] = template_ids
          if (page) {
            page.setData({
              template_ids: template_ids
            })
          }
          resolve(template_ids)
        })
      }
    })
  },

  /**
   * 隐藏tabbar
   */
  hidetabbar() {
    wx.hideTabBar({
      fail(err) {
        if (err.errMsg != 'hideTabBar:fail not TabBar page') {
          setTimeout(() => { // 做了个延时重试一次，作为保底。
            wx.hideTabBar()
          }, 500)
        }
      }
    })
  },

  /**
   * 设置tabbar
   */
  editTabbar() {
    this.hidetabbar()
    return new Promise((resolve, reject) => {
      let tabbar = JSON.parse(JSON.stringify(this.globalData.tabBar)),
        list = [];
      let pagePath = this.getPage().route;
      (pagePath.indexOf('/') != 0) && (pagePath = '/' + pagePath)
      for (let i = 0; i < tabbar.list.length; i++) {
        if (isTerminal() && i == 2) {
          continue
        } else if (isMerchants() && i > 0 && i < 4 && i != 2) {
          continue
        } else if (isSalesman() && i == 3) {
          continue
        } else {
          if (isMerchants() && i == 2) tabbar.list[i].text = '提取设备'
          tabbar.list[i].selected = false
          list.push(tabbar.list[i])
        }
      }
      for (let i = 0; i < list.length; i++) {
        (list[i].pagePath == pagePath) && (list[i].selected = true);
      }
      tabbar.list = list
      resolve(tabbar)
    })
  },

  /**
   * 获取页面
   */
  getPage(num = 1) {
    let pages = getCurrentPages(); //获取页面
    return pages[pages.length - num];
  },

  /**
   * 返回
   */
  goBack(delta = 1) {
    let pages = getCurrentPages(); //获取页面
    if (pages.length > delta) {
      wx.navigateBack({
        delta: delta
      });
    } else if (pages.length > 1) {
      wx.navigateBack({
        delta: 1
      });
    } else {
      wx.redirectTo({
        url: '/pages/index/index'
      })
    }
  },

  /**
   * 解码url参数
   */
  parseQuery() {
    const pages = getCurrentPages() //获取加载的页面
    const currentPage = pages[pages.length - 1] //获取当前页面的对象
    const options = currentPage.options //如果要获取url中所带的参数可以查看options

    if (options.scene) {
      const scene = decodeURIComponent(options.scene);
      let reg = /([^=&\s]+)[=\s]*([^&\s]*)/g;
      let obj = {};
      while (reg.exec(scene)) {
        obj[RegExp.$1] = RegExp.$2;
      }
      return obj;
    } else {
      return options;
    }
  },

  /**
   * 获取指定url参数
   */
  getQuery(url, key = ['sn', 't', 'n', 'dev_no']) {
    if (!url || (url.indexOf('?') == -1 && url.lastIndexOf('\/') == -1)) return {};
    var req = {};
    if (url.indexOf('?') > -1) {
      var query = url.split('?')[1].split('&');
      for (var i = 0; i < query.length; i++) {
        var qi = query[i].split("=");
        req[qi[0]] = qi[1];
        if (key.indexOf(qi[0]) > -1) return qi[1];
      }
      return req
    } else {
      return url.substring(url.lastIndexOf('\/') + 1, url.length)
    }
  },

  /**
   * 用户get请求
   */
  get(url, json = {}) {
    json.user_id = wx.getStorageSync('user_id') || '';
    json.token = wx.getStorageSync('token') || '';
    return this.request('GET', url, json, 1)
  },

  /**
   * 用户post请求
   */
  post(url, json = {}) {
    json.user_id = wx.getStorageSync('user_id') || '';
    json.token = wx.getStorageSync('token') || '';
    return this.request('POST', url, json, 1)
  },

  /**
   * 代理商get请求
   */
  agentGet(url, json = {}) {
    json.user_id = wx.getStorageSync('shopUser_id') || '';
    json.token = wx.getStorageSync('shopToken') || '';
    return this.request('GET', url, json, 2)
  },

  /**
   * 代理商post请求
   */
  agentPost(url, json = {}) {
    json.user_id = wx.getStorageSync('shopUser_id') || '';
    json.token = wx.getStorageSync('shopToken') || '';
    return this.request('POST', url, json, 2)
  },

  /**
   * request 请求封装
   */
  request(method, url, json, type, conten_type = 'application/json', ) {
    const that = this;
    json.appid = this.globalData.app_id || '';
    json.agent_id = this.globalData.agent_id || '';
    console.log(this.globalData.requestUrl)
    this.globalData.requestUrl = 'https://www.cnmonster.cn/'
    return new Promise(function (resolve, reject) {
      wx.request({
        url: that.globalData.requestUrl + url,
        data: json,
        header: {
          'content-type': conten_type
        },
        method: method,
        success(res) {
          let data = res.data;
          // 回调成功执行resolve
          if (json.otherCode == 2) {
            resolve(data) // 返回已经  便于拿msg
          } else if (json.otherCode && data.msg != '验证失败，无效的token' && data.msg != '登陆已过期，请重新登陆') {
            resolve(data) // 返回已经  便于拿msg
          } else if (data.code == 0) {
            resolve(data.data)
          } else if (data.msg == '验证失败，无效的token' || data.msg == '登陆已过期，请重新登陆') {
            if (type == 1) {
              if (json.login == 2) {
                reject(data)
              } else {
                that.checkLogin()
                reject(data)
              }
            } else if (type == 2) {
              wx.redirectTo({
                url: '/pages/agent/login/login',
              })
            }
          } else {
            reject(data)
            wx.showToast({
              icon: 'none',
              title: data.msg,
              duration: 3000
            })
          }
        },
        fail(data) {
          // 回调失败时
          if (data.errMsg == 'request:fail timeout') {
            wx.reLaunch({
              url: '/' + that.getCurrentPageUrl(2),
            })
          } else if (typeof reject == 'function') {
            reject(data);
          } else {
            wx.showToast({
              title: data.errMsg
            })
          }
        },
        complete(err) {

        }
      })
    });
  },

  /**
   * 去登录
   */
  checkLogin(type = 1) {
    if (!this.globalData.loginStatus) {
      this.globalData.loginStatus = true
      let url = this.getCurrentPageUrl(2)
      wx.setStorageSync('loginUrl', url)
      wx.redirectTo({
        url: '/pages/login/index',
      })
      // wx.hideLoading()
    }
  },

  /**
   * 获取当前页url
   * type 1 不带参数 2 带参数 3 返回参数
   */
  getCurrentPageUrl(type = 1) {
    var pages = getCurrentPages() //获取加载的页面
    var currentPage = pages[pages.length - 1] //获取当前页面的对象
    var url = currentPage.route //当前页面url
    var options = currentPage.options //如果要获取url中所带的参数可以查看options

    if (type == 1) {
      return url
    }
    if (type == 3) {
      return options
    } else {
      //拼接url的参数
      var urlWithArgs = url + '?'
      for (var key in options) {
        var value = options[key]
        urlWithArgs += key + '=' + value + '&'
      }
      urlWithArgs = urlWithArgs.substring(0, urlWithArgs.length - 1)
      return urlWithArgs
    }
  },

  /**
   * 默认分享
   */
  defauleSetShare(share_id = 1, obj = {}) {
    let pages = this.getCurrentPageUrl(2)
    obj = Object.assign(this.globalData.siteShare[share_id], obj)
    let title = obj.title || `欢迎您使用${this.globalData.siteInfo.mini_name}，让你的手机永远不断电`,
      path = obj.path || pages,
      imageUrl = obj.imageUrl || ''
    return {
      title,
      path,
      imageUrl
    }
  },

  /**
   * 默认分享
   */
  timeShare(share_id = 1, obj = {}) {
    obj = Object.assign(this.globalData.siteShare[share_id], obj)
    let title = obj.title || `欢迎您使用${this.globalData.siteInfo.mini_name}，让你的手机永远不断电`,
      query = obj.query || this.getCurrentPageUrl(3),
      imageUrl = obj.imageUrl || ''
    return {
      title,
      query,
      imageUrl
    }
  },

  /**
   * 代理默认分享
   */
  agentSetShare(share_id = 4, obj = {}) {
    let pages = this.getCurrentPageUrl(2)
    obj = Object.assign(this.globalData.siteShare[share_id], obj)
    let title = obj.title || `欢迎您！来到${this.globalData.siteInfo.mini_name}微信管理后台`,
      path = obj.path || pages,
      imageUrl = obj.imageUrl || this.globalData.ossUrl + '112629210.jpg'
    return {
      title,
      path,
      imageUrl
    }
  },

  /**
   * 代理默认分享朋友圈
   */
  agentTimeShare(share_id = 4, obj = {}) {
    obj = Object.assign(this.globalData.siteShare[share_id], obj)
    let title = obj.title || `欢迎您！来到${this.globalData.siteInfo.mini_name}微信管理后台`,
      query = obj.query || this.getCurrentPageUrl(3),
      imageUrl = obj.imageUrl || ''
    return {
      title,
      query,
      imageUrl
    }
  },

  
})