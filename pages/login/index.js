//授权页面
const app = getApp();
Page({
  data: {
    siteInfo: '',
  },
  onLoad(options) {
    console.log(options)
    let that = this
    app.getSiteInfo().then(res => {
      console.log(res)
      if (res.user_direct_login == 1) that.checkLogin()
    })
    console.log(1111)
    this.setData({
      canIUseGetUserProfile: wx.getUserProfile ? true : false
    })
    console.log(this.data)
    this.checkLogin()
  },

  /**
   * 检测是否登录过
   */
  checkLogin() {
    let that = this
    wx.login({
      success(res) {
        console.log(res)
        that.setData({
          loginCode: res.code
        })
        // const code = res.code
        wx.request({
          url: 'https://www.cnmonster.cn/stores/login/slogin',
          data: {
            code: res.code,
            login_type: 1
          },
          header: {
            'content-type': 'application/json' // 默认值
          },
          success(res) {
            console.log(res)
          var data = res.data.data
          console.log(res.data.s_state)
          if( res.data.s_state == 0){
            that.setData({
              s_state:0
            })
            wx.showToast({
              title: '未找到登录权限',
              duration: 5000,
              success:function(){
                setTimeout(() => {
                  wx.redirectTo({
                    url: '/pages/home/home'
                })  
                }, 3000);
                 
              }
            })
          }
          else if( data == null){
            that.setData({
              opendid : res.data.session_key.openid,
              smobile:''
            })   
            that.getPhoneNumber()
           
          }
            that.setData({
              opendid : res.data.session_key.openid,
              smobile : data.s_username,
            })
            console.log(that.data)
            app.globalData={
              opendid: res.data.session_key.openid,
              smobile: data.s_username,
            }
            console.log(app.globalData)
            that.tohomes()
          }
        })
      }
    })
  },

  /**
   * 获取用户信息
   */
  getUserProfile(e) {
    let that = this
    if (that.clickBtn) return
    that.clickBtn = true
    // 推荐使用wx.getUserProfile获取用户信息，开发者每次通过该接口获取用户个人信息均需用户确认
    wx.getUserProfile({
      desc: '用于完善您的个人信息', // 声明获取用户个人信息后的用途，后续会展示在弹窗中，请谨慎填写
      success: (res) => {
        console.log(res)
        if (that.data.siteInfo.register_user_moblie_wx == 0) {
          that.setData({
            encryptedData: encodeURIComponent(res.encryptedData),
            iv: res.iv
          })

          that.tohomes()
        } else {
          that.setData({
            encryptedData: encodeURIComponent(res.encryptedData),
            iv: res.iv,
            bindPhone: true
          })
          console.log(1111)
          console.log(that.setData)
          that.tohomes()
        }
      },
      
      complete() {
        that.clickBtn = false
      }
    })
  },
  /**
   * 获取用户手机号码
   */
  getPhoneNumber(e) {
    console.log(44444)
    console.log(e)
    let that = this
    if (e.detail.errMsg == 'getPhoneNumber:ok') {
      wx.request({
        url: 'https://www.cnmonster.cn/stores/Login/getphonenumber',
        data: {
          "code": e.detail.code,
          "openid":that.data.opendid
        },
        success: (res) => {
          console.log(res.data.phoneNumber);
          console.log(res.data)
          app.globalData = {
            smobile:res.data.phoneNumber 
          }
          that.tohomes()
        }
      })
    } else {
      wx.showModal({
        title: 'Sorry',
        content: '很遗憾，您拒绝了微信授权将无法使用本系统！',
        showCancel: false,
        success(res) {
          if (res.confirm) {
            wx.redirectTo({
              url: '/pages/login/index'
            })
          }
        }
      })
    }
  },
  tohomes(){
    console.log(this.data)
    console.log(app.globalData.opendid)
    // var openid_id = app.globalData.opendid
    // var smobile = app.globalData.smobile
    var openid_id = 'oemoI5k-qDpMPQdmRKVb_vNq6fCg'
    var smobile = '13839561859'
    wx.redirectTo({
      url: '/pages/index/index?openid_id='+openid_id +'&smobile=' +smobile, // 目标页面的路径，并传递参数
      
    })
  }
})