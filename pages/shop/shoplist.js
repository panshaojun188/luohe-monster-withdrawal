const app = getApp()

Page({

  /**
   * 页面的初始数据
   */
  data: {

  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    console.log(app.globalData)
    this.getshoplist()
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  },
  getshoplist(){
    var that = this
 let s_username = app.globalData.s_username
  wx.request({
    url: 'https://www.cnmonster.cn/wxapi/months_list_or',
    method: 'GET', // 默认为GET，有效值：OPTIONS, GET, HEAD, POST, PUT, DELETE, TRACE, CONNECT
    data: { 
      s_username:s_username
    }, // 需要发送给服务器的数据
    header: { // 设置请求的header
      'content-type': 'application/json' // 默认为 application/json
    },
    success(res) {
      console.log(res.data.data) // 返回的数据
      that.setData({
       shopList:res.data.data
      })
  
    },
    fail(err) {
      // 请求失败处理
    },
    complete() {
      // 请求完成后的处理，无论成功还是失败都会执行
    }
  })
  },
  /**
   * 跳转页面
   */
  toJump(e) {
    console.log(e)
    app.globalData.user_id = e.currentTarget.dataset.uid
    app.globalData.user_money = e.currentTarget.dataset.money
    app.globalData.user_name = e.currentTarget.dataset.name
    wx.setStorageSync('user_id', e.currentTarget.dataset.uid)
    wx.navigateTo({
      url: e.currentTarget.dataset.url
    })
  },
})