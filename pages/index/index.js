const app = getApp()

// 在页面中定义插屏广告
let interstitialAd = null
import {

  goodsIcon
} from '../../utils/util.js'
Page({
  data: {
    Custom: app.globalData.Custom,
    scale: 16,
    markers: [],
    total_money: '0.00',
    mobile_phone: '',
    initLoad: true,
    locInfo: {},



  },

  onReady(e) {

    // this.mapCtx = wx.createMapContext('map')
  },

  //事件处理函数
  onLoad(options) {

    // 在页面onLoad回调事件中创建插屏广告实例
    if (wx.createInterstitialAd) {
      interstitialAd = wx.createInterstitialAd({
        adUnitId: 'adunit-22ad02f4510db869'
      })
      interstitialAd.onLoad(() => {})
      interstitialAd.onError((err) => {})
      interstitialAd.onClose(() => {})
    }
    //  var that = this
    console.log(app.globalData)
    console.log(options)
    this.setData({
      opendid: app.globalData.opendid,
      smobile: app.globalData.smobile,
      // opendid: 'oemoI5k-qDpMPQdmRKVb_vNq6fCg',
      // smobile: '18639563977',
    })
    console.log(this.data)
    this.getShoplist()
  },

  /**
   * 获取商户列表
   */
  getShoplist() {
    // 在适合的场景显示插屏广告
    if (interstitialAd) {
      interstitialAd.show().catch((err) => {
        console.error(err)
      })
    }
    let that = this
    console.log(that.data.smobile)
    app.globalData.s_username = that.data.smobile
    app.globalData.s_x_openid = that.data.opendid
    wx.request({
      url: 'https://www.cnmonster.cn/stores/login/near_stores', //仅为示例，并非真实的接口地址
      method: 'POST',
      data: {
        mobile_phone: that.data.smobile
      },
      header: {
        'content-type': 'application/x-www-form-urlencoded' // 默认值
      },
      success(res) {
        console.log(app.globalData)
        console.log(res.data)
        app.globalData.store_min = res.data.data.store_min
        app.globalData.serv_chaneg = res.data.data.serv_chaneg
        app.globalData.cantixian = res.data.data.total_money
        app.globalData.store_money = res.data.store_money
        app.globalData.tishi = res.data.tishi
        app.globalData.showModal = res.data.showModal
        console.log(res.data.data.store_info_aa.length)
        if (res.data.data.store_info_aa.length == 0) {
          wx.showToast({
            title: '未找到归属于门店信息',
            duration: 2000,
            success: function () {
              setTimeout(function () {
                //要延时执行的代码
                wx.navigateTo({
                  url: '/pages/home/home',
                })
              }, 500) //延迟时间
            }
          })
        }
        var res = res.data.data
        that.setData({
          sm_mobile:res.sm.sm_mobile,
          sm_name:res.sm.sm_name,
          length_store: res.length_store,
          store_info_aa: res.list,
          orders_stores_money: res.orders_stores_money,
          shopList: res.store_info_aa,
          total_money: res.total_money,
          mobile_phone: res.mobile_phone,
          no_orders: res.no_order,
          store_money: app.globalData.store_money
        })
      }
    })




  },


  /**
   * 跳转页面
   */
  toJump(e) {
    console.log(e)
    app.globalData.user_id = e.currentTarget.dataset.uid
    app.globalData.user_money = e.currentTarget.dataset.money
    app.globalData.user_name = e.currentTarget.dataset.name
    wx.setStorageSync('user_id', e.currentTarget.dataset.uid)
    wx.navigateTo({
      url: e.currentTarget.dataset.url
    })
  },
  // 总体提现/pages/me/withdraw/account
  totixian(e) {
    console.log(e)
    app.globalData.user_money = e.currentTarget.dataset.money
    app.globalData.user_name = e.currentTarget.dataset.username
    wx.setStorageSync('user_id', e.currentTarget.dataset.username)
    wx.navigateTo({
      url: e.currentTarget.dataset.url
    })

  },

  /**
   * 展示店铺信息
   * @param {Object} e
   */
  showShop(e) {
    let {
      shopList
    } = this.data
    this.setData({
      shopInfo: shopList[e.detail.markerId]
    })
  },

  hideShop() {
    this.setData({
      shopInfo: ''
    })
  },
  makePhone() {
    wx.makePhoneCall({
      phoneNumber: app.globalData.siteInfo.service_phone
    })
  },
  toCashOut: function toCashOut(e) {

    wx.navigateTo({
      url: "/pages/me/withdraw/account_withdraw"
    });
  },
  toCashOutLog: function toCashOutLog(e) {
    //去提现记录
    wx.navigateTo({
      url: "/pages/me/withdraw/wlist"
    });
  },
  toStoreList: function toStoreList(e) {
    //门店列表
    wx.navigateTo({
      url: "/pages/shop/shoplist"
    });
  },
  toBankCard: function toBankCard(e) {
    wx.navigateTo({
      url: "/pages/me/withdraw/account"
    });
    return;
    //去银行卡管理
    wx.navigateTo({
      url: "/pages/bankCard/bankCard"
    });
  },
  toMyInformation: function toMyInformation(e) {
    //我的信息,商户信息
    wx.navigateTo({
      url: "/pages/myInformation/myInformation"
    });
  },
  toRightsInterests: function toRightsInterests(e) {
    //商家权益
    // wx.showToast({
    //     title: '未开启权益功能',
    //     icon: 'none'
    // })
    // return;
    wx.navigateTo({
      url: "/pages/rightsInterests/rightsInterests"
    });
  },
  toRecommendation: function toRecommendation(e) {
    //合作推荐recommendation
    wx.navigateTo({
      url: "/pages/recommendation/recommendation"
    });
  },
  toMyDevices: function toMyDevices(e) {
    //去我的设备管理
    wx.navigateTo({
      url: "/pages/myDevices/myDevices"
    });
  },
  toBillMonth: function toBillMonth(e) {
    wx.navigateTo({
      url: "/pages/me/finance/index"
    });
    /*wx.switchTab({
        url: '/pages/index/index',
    })*/
    //月报表 查询收益
    // wx.navigateTo({
    //     url: '/pages/billMonth/billMonth'
    // });
  },
  callPhone: function callPhone(e) {
    //拨打客服电话
    var mobile = e.currentTarget.dataset.phone;
    wx.makePhoneCall({
      phoneNumber: mobile
    });
  },
  getUserInfo: function getUserInfo() {
    var that = this;
    wx.request({
      url: app.globalData.domain + "/tools/MallSelect.ashx",
      data: {
        action: "getUserInfo_GS",
        sitenum: app.globalData.sitenum,
        user: app.globalData.account,
        token: ""
      },
      method: "get",
      success: function success(res) {
        console.log(res);
        if (res.statusCode == 200 && res.data.status == 1) {
          if (res.data.data) {
            that.setData({
              userInfo: res.data
            });
          }
        } else {
          wx.showToast({
            title: res.data.msg,
            icon: "error"
          });
        }
      },
      fail: function fail(r1, r2, r3) {}
    });
  }

})