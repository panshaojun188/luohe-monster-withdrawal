// 在页面中定义插屏广告
let interstitialAd = null
Page({
    data: {
        banner: [{
            img_url: "/images/001.jpg",
            title: "场景展示1"
        }, {
            img_url: "/images/002.jpg",
            title: "场景展示2"
        }, {
            img_url: "/images/003.jpg",
            title: "场景展示2"
        }, {
            img_url: "/images/004.jpg",
            title: "场景展示4"
        }]
    },
    onLoad: function() {
        // 在页面onLoad回调事件中创建插屏广告实例
        if (wx.createInterstitialAd) {
            interstitialAd = wx.createInterstitialAd({
                adUnitId: 'adunit-bee988507d84990d'
            })
            interstitialAd.onLoad(() => {})
            interstitialAd.onError((err) => {})
            interstitialAd.onClose(() => {})
        }
    },

    onShow: function onShow() {
        //当用户打开的小程序最底层页面是非首页时，默认展示“返回首页”按钮 在页面 onShow 中调用 hideHomeButton 方法 进行隐藏
        wx.hideHomeButton();
    },
    onShareAppMessage: function onShareAppMessage() {
        return {};
    },
    // 在适合的场景显示插屏广告
    if (interstitialAd) {
        interstitialAd.show().catch((err) => {
            console.error(err)
        })
    },
    toLogin: function toLogin(e) {
        //去登录
        wx.navigateTo({
            url: "/pages/login/index"
        });
    },
  
});