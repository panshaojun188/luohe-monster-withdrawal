// pages/order_list/order.js
const app = getApp()
import {
  frontGoodsType
} from '../../../utils/util.js'
Page({

  /**
   * 页面的初始数据
   */
  data: { 
	  
	
    TabCur: 99,
    goods_type: -1,
    pros: [],
    pages: {},
    noinfo: "---",
    goodsType: {},
    typeStatus: {
      1: '微信零钱', 2: '微信银行卡',  3: '支付宝转账', 4: '人工转账'
    },
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad (options) {
   app.globalData.requestUrl="https://www.cnmonster.cn/"
    app.getSiteInfo().then(res => {
      this.setData({
        device_length: res.device_type.length,
        tab: Object.assign({ '全部': 99 }, frontGoodsType()),
        goodsType: frontGoodsType(4)
      })
    })
 
    this.getList()
	
  },

  

  /**
   * 获取列表
   */
  getList(index = 0) {
    var self = this,  url = 'wxapi/account_list_or', params = {}
   console.log(app.globalData)
  params.s_username = app.globalData.s_username
   console.log(params)
    app.get(url, params).then(res => {
		console.log(res)
      self.setData({
        list: res.list,
        s_username :app.globalData.s_username
      });
    });
  },

  /**
  * 页面上拉触底事件的处理函数
  */
  onReachBottom() {
    var self = this, pData = this.data;
    if (pData.pages[pData.TabCur].flag) return;
    pData.pages[pData.TabCur].flag = true;
    self.setData({ pages: pData.pages });
    self.getList(pData.TabCur);
  },



  accountdel (e) {
    let that = this, id = e.currentTarget.dataset.id,pData = that.data, url = 'wxapi/account_del', getparams = {};
    console.log('id',id)
	  wx.showModal({
      title: '',
      content: '如果此账号有提现记录，则无法删除，确定要删除该账号？',
      success: function (sm) { 
			if (sm.confirm) {
				app.get(url, {id: id}).then(res => {
          console.log(res)
					if (res.code == 0) {
						wx.showToast({
							type: 'success',
							title: res.msg
						})
						that.onLoad();
        }
        if(res.code ==1){
          wx.showToast({
							type: 'error',
							title: res.msg
            })
            that.onLoad();
        }
        
				})
				
			} else if (sm.cancel) {
				console.log('用户点击取消')
			}
        }
      })

    /* wx.navigateTo({
      url: "/pages/me/finance/day?month=" + month
    }) */
  }
})