// pages/order_list/order.js
const app = getApp()
import {
  frontGoodsType,
  getSetValueObj
} from '../../../utils/util.js'
Page({

  /**
   * 页面的初始数据
   */
  data: {
    form: {
      issueType: 1
    },
    form1: {},
    issueArr: [{
        type: 1,
        name: '微信零钱'
      },
      {
        type: 2,
        name: '微信银行卡'
      }

    ],
    waysArr: {},
    index: 0,
    index1: 0,
    ac_id: 0,
    ac_type: 1,
    can_apply_money: '0.00',
    orders_store_money: '0.00',
    placeholder: '',
    choosedBank: null,
    now_can_apply: '',
    //选中的银行卡对象, 数组
    bankcardList: [],
    //银行卡列表
    tixianType: "weixin",
    userInfo: {},
    txjeInput: "",
    tixianFlag: false,
    isusertel: true,
    weixin_CashOutDeductionRatio: 1,
    bank_CashOutDeductionRatio: 1,
    store_max: 0,
    store_min: 0,
    showModal: app.globalData.showModal, 
    // tishi :'  受春节放假影响,提现到账时间按工作日顺延!祝您春节快乐!龙年生意兴隆!',
    logimage : '怪兽充电',
    tishi:app.globalData.tishi,
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    console.log(options)
    console.log(app.globalData)
    this.setData({
      // 'form.issueType': options.issueType || 0
      'form.issueType': 0,
      rec_account:app.globalData.opendid,
      s_username:app.globalData.s_username,
      can_apply_money:app.globalData.cantixian,
      Service_Charge:app.globalData.serv_chaneg,
      store_min:app.globalData.store_min,
      store_max:3000
      
    })
    console.log(this.data)
    app.getSiteInfo()
    this.getWays()
    console.log(this.data.waysArr)
  },
  // 在需要显示模态窗口的时候调用该方法
  showModal: function() {
    this.setData({
      showModal: true
    });
  },

  // 在模态窗口点击确定或取消时调用该方法
  hideModal: function() {
    this.setData({
      showModal: false
    });
  },
 
  /**
   * 获取列表
   */
  getWays() {
    console.log(this.data.tixianType)
    var that = this
  
    console.log(that.data)
  },
  tixianTypeHandler: function tixianTypeHandler(e) {
    console.log(this.data)
    console.log(e);
    var tixianType = e.target.dataset.type;
    this.setData({
      tixianType: tixianType
    });
  },
  tixianTypeHandlers: function tixianTypeHandlers(e) {
    console.log(e);
    var tixianType = e.target.dataset.type;
    this.setData({
      tixianType: tixianType,
      ac_type: 2
    });
  },
  /**
   * 
   * @param {设置值} e 
   */
  setValue(e) {
    this.setData(getSetValueObj(e))
  },
  bindPickerChange: function (e) {
    console.log(e);
    // console.log(this.data.waysArr[e.detail.value].ac_id)
    // var mchObj = this.data.waysArr[e.detail.value];
    this.setData({
      index1: e.detail.value,
      ac_type: e.detail.value,
      // ac_id: mchObj.ac_id,

    }) /* */
    console.log(this.data)
  },
  /**
   * 提交铺货
   */
  formSubmit(e) {

    console.log(e.detail)
    console.log(this.data.rec_account)
   var apply_money = e.detail.value.apply_money
   console.log(apply_money)
   console.log(app.globalData)
    if (apply_money <= 0) {
      console.log(222222)
      wx.showLoading({
        mask: true,
        title: '请填写提现金额'
      })
      return
    }
    console.log(this.data.can_apply_money)
    if (Number(apply_money) > Number(this.data.can_apply_money)) {
      console.log(222222)
      console.log(typeof apply_money)
      console.log(typeof this.data.can_apply_money)
      wx.showLoading({
        mask: true,
        title: '亲,金额太多了'
      })
      return
    }
    if (parseFloat(apply_money) < parseFloat(this.data.store_min)) {
     
      wx.showLoading({
        mask: true,
        title: '小于最低限额'
      })
      return
    }
    if (parseFloat(apply_money) > parseFloat(this.data.store_max)) {
     
      wx.showLoading({
        mask: true,
        title: '大于最高限额'
      })
      return
    }

    if (this.data.rec_account.ac_bank != "微信") {
      this.setData({
        ac_type: 2,
      });
    }
    let that = this   
    wx.showLoading({
      mask: true,
      title: '正在提交~~~'
    })

    wx.request({
      url: 'https://www.cnmonster.cn/wxapi/account_ways_or',
      method: 'POST',
      data: {
        s_username: app.globalData.s_username,
        tixian: that.data.tixianType,
        rec_account:app.globalData.opendid,
        can_apply_money:apply_money,
        Service_Charge:app.globalData.serv_chaneg,
      },
      header: {
        'content-type': 'application/x-www-form-urlencoded' // 默认值
      },
      success(e) {
        console.log(e)
        let res = e.data
        if(res.code == 1){
          app.globalData.cantixian = app.globalData.cantixian - apply_money
          setTimeout(() => {
            wx.navigateTo({
              url: '/pages/index/index'
            })
          }, 3000)
        }
      }
    });
  },
  accountcreate() {
    let that = this,
      url = 'wxapi/account_create';

    app.get(url, {
      type: that.data.issueArr[that.data.form.issueType].type
    }).then(res => {
      if (res.code == 0) {
        wx.showToast({
          type: 'success',
          title: res.msg
        })

        setTimeout(() => {
          wx.navigateTo({
            url: '/pages/me/withdraw/account'
          })
        }, 1000)
        that.onLoad();

      } else {
        wx.showToast({
          type: 'fail',
          title: res.msg
        })

      } /*  */
    })

  }
})