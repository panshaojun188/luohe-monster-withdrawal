// pages/order_list/order.js
const app = getApp()
import {
  frontGoodsType,
  getSetValueObj
} from '../../../utils/util.js'
Page({

  /**
   * 页面的初始数据
   */
  data: {
    form: {
      issueType: 1
    },
    form1: {},
    issueArr: [{
        type: 1,
        name: '微信零钱'
      },
      {
        type: 2,
        name: '微信银行卡'
      }
      
    ],
    waysArr: {},
    index: 0,
    index1: 0,
    ac_id: 0,
    ac_type: 1,
    can_apply_money: '0.00',
    orders_store_money: '0.00',
    placeholder: '',
    choosedBank: null,
    // showModal:true,
     showModal: app.globalData.showModal, 
    // tishi :'  受春节放假影响,提现到账时间按工作日顺延!祝您春节快乐!龙年生意兴隆!',
    logimage : '怪兽充电',
    tishi:app.globalData.tishi,
    now_can_apply: '',
    //选中的银行卡对象, 数组
    bankcardList: [],
    //银行卡列表
    tixianType: "weixin",
    userInfo: {},
    txjeInput: "",
    tixianFlag: false,
    isusertel: true,
    rec_account:'',
    weixin_CashOutDeductionRatio: 1,
    bank_CashOutDeductionRatio: 1,
    store_max: 0,
    store_min: 0
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    console.log(options)
    this.setData({
      // 'form.issueType': options.issueType || 0
      'form.issueType': 0,
      can_apply_money:app.globalData.user_money,
      user_id:app.globalData.user_name,
      
    })
    app.getSiteInfo()
    console.log(this.data)
    console.log(app.globalData.user_name)
    console.log(app.globalData.user_money)
    this.getWays()
   
  },
  // 在需要显示模态窗口的时候调用该方法
  showModal: function() {
    this.setData({
      showModal: true
    });
  },

  // 在模态窗口点击确定或取消时调用该方法
  hideModal: function() {
    this.setData({
      showModal: false
    });
  },
 
  /**
   * 获取列表
   */
  getWays() {
    console.log(this.data.tixianType)
    console.log(app.globalData.user_name)
    var that = this
    var self= this,
      // url = 'wxapi/account_ways_store',
      params = app.globalData.user_name,
      tixian = that.data.tixianType
      wx.request({
        url: 'https://www.cnmonster.cn/wxapi/account_ways_store',
        method: 'GET',
        data: {
          user_id : app.globalData.user_name,
          tixian :that.data.tixianType
        },
        header: {
          'content-type': 'application/x-www-form-urlencoded' // 默认值
        },
        success(res) {
          console.log(res)
        var  res=res.data.data
          console.log(res)
          if (typeof res.rec_account === 'undefined' || res.rec_account === null || res.rec_account === ''){
            wx.showToast({
              title: '请先创建提现账号',
              duration: 2000,
            });
            setTimeout(function() {  
              wx.navigateTo({
                url: '/pages/index/index',
              }) 
            }, 2000);
            
          
            // return
          }
          that.setData({
            Service_Charge:res.Service_Charge,
            bankcardList:res.list_card,
           
            rec_account:res.rec_account,
            waysArr: res.list,
            placeholder: '可提现' + app.globalData.user_money,
            now_can_apply: app.globalData.user_money,
            bank_card_sc:res.bank_sc,
            store_max: res.store_max,
            store_min: res.store_min,
            can_daozhang:res.can_daozhang,
          });
        }
      });
   // app.get(url, params, tixian).then(res => {
      
      // console.log(res.list)
      // console.log(res.list_card[0])
     
    
   // });
    console.log(that.data)
   
  },
  tixianTypeHandler: function tixianTypeHandler(e) {
    console.log(this.data)
    console.log(e);
    var tixianType = e.target.dataset.type;
    this.setData({
      tixianType: tixianType,
      ac_type:1
    });
  },
  tixianTypeHandlers: function tixianTypeHandlers(e) {
    console.log(e);
    var tixianType = e.target.dataset.type;
    this.setData({
      tixianType: tixianType,
      ac_type:2
    });
  },
  /**
   * 
   * @param {设置值} e 
   */
  setValue(e) {
    this.setData(getSetValueObj(e))
  },
  bindPickerChange: function (e) {
    console.log(e);
     console.log(this.data.waysArr[e.detail.value].ac_id)
    // var mchObj = this.data.waysArr[e.detail.value];
    this.setData({
      index1: e.detail.value,
      ac_type: e.detail.value,
      // ac_id: mchObj.ac_id,

    }) /* */
    console.log(this.data)
  },
  /**
   * 提交铺货
   */
  formSubmit(e) {    
    console.log(e.detail.value)
    console.log(this.data.rec_account) 
    if(e.detail.value.apply_money <= 0){
      wx.showLoading({
        mask: true,
        title: '请填写提现金额'
      })
      return
    }
    if(e.detail.value.ac_bank_code == ''){
      wx.showLoading({
        mask: true,
        title: '请创建银行卡提现账号'
      })
      setTimeout(() => {
        wx.navigateTo({
          url: '/pages/index/index'
        })
      }, 1500)
      return
    }   
    console.log(this.data) 
    let acbank=this.data.rec_account.ac_bank
    console.log(acbank)
    if(this.data.rec_account =="微信转银行卡"){
      this.setData({
        ac_type:2,
      });
    }
    console.log(this.data.ac_type)
    console.log(this.data.bankcardList)
    let that = this,
      pData = this.data,
      url = 'wxapi/account_with_copy',
      params = e.detail.value
      params.rec_account = pData.rec_account
    params.ac_type = pData.ac_type
    console.log(params) 
    if(params.rec_account === ''){
      console.log(3333333)
      wx.showToast({
        title: '请创建提现账号'
      })
      setTimeout(() => {
        wx.navigateTo({
          url: '/pages/index/index'
        })
      }, 3000)
    }
    //params.ac_type = "1",
    console.log(11111111111111111111)
    console.log(params) 
    app.post('wxapi/account_with_copy', params).then(res => {
      // console.log(res)     
      wx.showToast({
        title: '提交成功'
      })
      setTimeout(() => {
        wx.navigateTo({
          url: '/pages/index/index'
        })
      }, 1500)
      console.log(222222)
    }) /* */
  },
  accountcreate() {
    let that = this,
      url = 'wxapi/account_create';
    app.get(url, {
      type: that.data.issueArr[that.data.form.issueType].type
    }).then(res => {
      //	console.log(res);
      if (res.code == 0) {
        wx.showToast({
          type: 'success',
          title: res.msg
        })
        setTimeout(() => {
          wx.navigateTo({
            url: '/pages/me/withdraw/account'
          })
        }, 1000)
        that.onLoad();

      } else {
        wx.showToast({
          type: 'fail',
          title: res.msg
        })

      } /*  */
    })

  }
})