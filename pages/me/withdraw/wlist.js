// pages/order_list/order.js
const app = getApp()
import {
  frontGoodsType
} from '../../../utils/util.js'
Page({

  /**
   * 页面的初始数据
   */
  data: { 
	  
	
    TabCur: 99,
    goods_type: -1,
    pros: [],
    pages: {},
    noinfo: "---",
    goodsType: {},
    typeStatus: {
      1: '微信零钱', 2: '微信银行卡',  3: '支付宝转账', 4: '人工转账'
    },
	recType: {
      1: '平台代付', 2: '代理自付'
    },
	wState: {
      '-1': '取消', 1: '未处理', 2: '已到账', 3: '失败'
    },
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad (options) {
    console.log(options)
    console.log(app.globalData)
   this.setData({
     rec_account: app.globalData.opendid,
     s_username:app.globalData.s_username
   })
   this.getList()
	
  },

  
  /**
   * 获取列表
   */
  getList(index = 0) {
    var  url = 'wxapi/account_wlist_or', params = {},that = this
    params.rec_account = this.data.rec_account
    params.s_username = this.data.s_username
    app.get(url, params).then(res => {
    console.log(res)
    that.setData({
      list:res.list
    })
     
      console.log(this.data)
    });
  },

  /**
  * 页面上拉触底事件的处理函数
  */
  onReachBottom() {
    var self = this, pData = this.data;
    if (pData.pages[pData.TabCur].flag) return;
    pData.pages[pData.TabCur].flag = true;
    self.setData({ pages: pData.pages });
    self.getList(pData.TabCur);
  },



  accountdel (e) {
    let that = this, id = e.currentTarget.dataset.id,pData = that.data, url = 'wxapi/account_del', getparams = {};
	wx.showModal({
      title: '',
      content: '确定要删除该账号？',
      success: function (sm) { 
			if (sm.confirm) {
				app.get(url, {id: id}).then(res => {
					if (res.code == 0) {
						wx.showToast({
							type: 'success',
							title: res.msg
						})
						that.onLoad();
				
						
					}
				})
				
			} else if (sm.cancel) {
				console.log('用户点击取消')
			}
        }
      })

    /* wx.navigateTo({
      url: "/pages/me/finance/day?month=" + month
    }) */
  }
})