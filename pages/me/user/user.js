var app = getApp();

Page({
  data: {
    userInfo: {},
    Custom: app.globalData.Custom,
    scale: 16,
    markers: [],
    total_money: '0.00',
    mobile_phone: '',
    initLoad: true,
    locInfo: {},
    can_apply_money: '0.00',
    orders_store_money: '0.00',
    
    goods_type: -1,

  },
  onLoad(options) {
    console.log(options)
    console.log(app.globalData)
    var that = this
    that.setData({
      user_money: app.globalData.user_money,
      user_name: app.globalData.user_name
    })
    console.log(this.data)
    app.getSiteInfo()
    console.log(app.getSiteInfo())
    this.getWays()
  },
  onShareAppMessage: function onShareAppMessage() {
    return {};
  },
  /**
   * 获取列表
   */
  getWays() {
    var self = this,
      url = 'wxapi/account_ways',
      params = {}

    app.get(url, params).then(res => {
      console.log(res)

      self.setData({
        can_apply_money:res.can_apply_money,
        waysArr: res.list,
        placeholder: '可提现' + res.can_apply_money + '，未出账单' + res.orders_store_money
      });
    });
  },
  // onShow: function onShow() {
  //   var that = this
  //   app.getUserInfo().then(res => {
  //     if (that.data.initLoad) {
  //       that.setData({
  //         initLoad: false
  //       })
  //       this.getShoplist()

  //     }
  //   }).catch(err => {
  //     //this.getShoplist()
  //   })
  // },
  toIndex: function toIndex(e) {
    //去首页
    wx.navigateTo({
      url: "/pages/index/index"
    });
  },
  toCashOut: function toCashOut(e) {
    wx.navigateTo({
      url: "/pages/me/withdraw/account_withdraw"
    });
  },
  toCashOutLog: function toCashOutLog(e) {
    //去提现记录
    wx.navigateTo({
      url: "/pages/me/withdraw/wlist"
    });
  },
  toStoreList: function toStoreList(e) {
    //门店列表
    wx.navigateTo({
      url: "/pages/index/index"
    });
  },
  toBankCard: function toBankCard(e) {
    wx.navigateTo({
      url: "/pages/me/withdraw/account"
      });
    return;
    //去银行卡管理
    wx.navigateTo({
      url: "/pages/bankCard/bankCard"
    });
  },
  toMyInformation: function toMyInformation(e) {
    //我的信息,商户信息
    wx.navigateTo({
      url: "/pages/myInformation/myInformation"
    });
  },
  toRightsInterests: function toRightsInterests(e) {
    //商家权益
    // wx.showToast({
    //     title: '未开启权益功能',
    //     icon: 'none'
    // })
    // return;
    wx.navigateTo({
      url: "/pages/rightsInterests/rightsInterests"
    });
  },
  toRecommendation: function toRecommendation(e) {
    //合作推荐recommendation
    wx.navigateTo({
      url: "/pages/recommendation/recommendation"
    });
  },
  toMyDevices: function toMyDevices(e) {
    //去我的设备管理
    wx.navigateTo({
      url: "/pages/myDevices/myDevices"
    });
  },
  toBillMonth: function toBillMonth(e) {
    wx.navigateTo({
      url: "/pages/me/finance/index"
    });
    /*wx.switchTab({
        url: '/pages/index/index',
    })*/
    //月报表 查询收益
    // wx.navigateTo({
    //     url: '/pages/billMonth/billMonth'
    // });
  },
  callPhone: function callPhone(e) {
    //拨打客服电话
    var mobile = e.currentTarget.dataset.phone;
    wx.makePhoneCall({
      phoneNumber: mobile
    });
  },
  getUserInfo: function getUserInfo() {
    var that = this;
    wx.request({
      url: app.globalData.domain + "/tools/MallSelect.ashx",
      data: {
        action: "getUserInfo_GS",
        sitenum: app.globalData.sitenum,
        user: app.globalData.account,
        token: ""
      },
      method: "get",
      success: function success(res) {
        console.log(res);
        if (res.statusCode == 200 && res.data.status == 1) {
          if (res.data.data) {
            that.setData({
              userInfo: res.data
            });
          }
        } else {
          wx.showToast({
            title: res.data.msg,
            icon: "error"
          });
        }
      },
      fail: function fail(r1, r2, r3) {}
    });
  }
});