// pages/order_list/order.js
const app = getApp()
import {
  frontGoodsType
} from '../../../utils/util.js'
Page({

  /**
   * 页面的初始数据
   */
  data: {
    text: "温馨提示：订单只保留最近三个月",
    marqueePace: 1, //滚动速度
    marqueeDistance: wx.getSystemInfoSync().windowWidth, //初始滚动距离
    marquee_margin: wx.getSystemInfoSync().windowWidth,
    size: 16,
    interval: 25, // 时间间隔  
    orders_store_money: '0.00',
    percent: 0,
    month1: 0,
    TabCur: 99,
    goods_type: -1,
    pros: [],
    pages: {},
    noinfo: "---",
    goodsType: {},
    orderStatus: {
      '-1': '已取消',
      0: '未启动',
      1: '进行中',
      2: '已完成',
      3: '手动完结',
      4: '异常',
      5: '超时完成'
    },
    refundStatus: {
      1: '申请中',
      2: '已拒绝',
      3: '已通过',
      4: '已退款'
    }
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    console.log(app.globalData)
    this.order_type = options.order_type || 4
    app.getSiteInfo().then(res => {
      this.setData({
        device_length: res.device_type.length,
        tab: Object.assign({
          '全部': 99
        }, frontGoodsType()),
        goodsType: frontGoodsType(4)
      })
    })
    this.loadProList(this.data.TabCur)
  },
  onShow: function () {
    var that = this;
    var length = that.data.text.length * that.data.size; //文字长度
    var windowWidth = wx.getSystemInfoSync().windowWidth; // 屏幕宽度
    //console.log(length,windowWidth);
    that.setData({
      length: length,
      windowWidth: windowWidth
    });
    that.scrolltxt(); // 第一个字消失后立即从右边出现
  },

  scrolltxt: function () {
    var that = this;
    var length = that.data.length; //滚动文字的宽度
    var windowWidth = that.data.windowWidth; //屏幕宽度
    if (length < windowWidth) {
      var interval = setInterval(function () {
        var maxscrollwidth = length + that.data.marquee_margin; //滚动的最大宽度，文字宽度+间距，如果需要一行文字滚完后再显示第二行可以修改marquee_margin值等于windowWidth即可
        var crentleft = that.data.marqueeDistance;
        if (crentleft < maxscrollwidth) { //判断是否滚动到最大宽度
          that.setData({
            marqueeDistance: crentleft + that.data.marqueePace
          })
        } else {
          //console.log("替换");
          that.setData({
            marqueeDistance: 0 // 直接重新滚动
          });
          clearInterval(interval);
          that.scrolltxt();
        }
      }, that.data.interval);
    } else {
      that.setData({
        marquee_margin: "500"
      }); //只显示一条不滚动右边间距加大，防止重复显示
    }
  },
  /**
   * 加载更多
   */
  loadProList(index = 0) {
    var pData = this.data;
    pData.pages[index] = {
      page: 0,
      flag: true
    }
    this.setData({
      pages: pData.pages
    })
    this.getList(index)
  },

  /**
   * 获取列表
   */
  getList(index = 0) {
    var self = this,
      pData = self.data,
      pages = pData.pages,
      url = 'wxapi/months_list',
      params = {}
    params.start = pages[index].page
    pages[index].page++
    params.depend_type = pData.goods_type
    params.order_type = self.order_type
    app.get(url, params).then(res => {
      pages[index].totalPage = res.page_num;
      if (res.page_num > pages[index].page) {
        pages[index].flag = false;
      }
      self.setData({
        [`pros[${index}][${pages[index].page - 1}]`]: res.list,
        orders_store_money: res.orders_store_money,
        percent: res.percent,
        month1: res.month1,
        pages: pages
      });
    });
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {
    var self = this,
      pData = this.data;
    if (pData.pages[pData.TabCur].flag) return;
    pData.pages[pData.TabCur].flag = true;
    self.setData({
      pages: pData.pages
    });
    self.getList(pData.TabCur);
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {
    return app.defauleSetShare();
  },

  /**
   * 用户点击右上角分享朋友圈
   */
  onShareTimeline() {
    return app.timeShare()
  },


  orderto(e) {
    let that = this,
      month = e.currentTarget.dataset.month;
    //console.log(e.currentTarget.dataset.month);
    wx.navigateTo({
      url: "/pages/me/finance/day?month=" + month
    })
  },

  /**
   * 设备保修
   */
  repair(e) {
    let that = this,
      item = e.currentTarget.dataset.item;
    wx.showModal({
      title: '设备报修',
      content: '确定该设备未能正常使用吗？',
      success(res) {
        if (res.confirm) {
          app.post('wxapi/save_repaire_device', {
            goods_sn: item.goods_sn,
            order_id: item.order_id
          }).then(res => {
            wx.showToast({
              title: '提交报修成功'
            })
          })
        }
      }
    })
  }
})