// pages/order_list/order.js
const app = getApp()
import {
  frontGoodsType
} from '../../../utils/util.js'
Page({

  /**
   * 页面的初始数据
   */
  data: {
	
	month2:0,	  
	orders_store_money:'0.00',
	percent:0,
	month1:0,
    TabCur: 99,
    goods_type: -1,
    pros: [],
    pages: {},
    noinfo: "---",
    goodsType: {}
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad (options) {
	  this.month = options.month;
    this.order_type = options.order_type || 4
    app.getSiteInfo().then(res => {
      console.log(res)
      this.setData({
        device_length: res.device_type.length,
        tab: Object.assign({ '全部': 99 }, frontGoodsType()),
        goodsType: frontGoodsType(4)
      })
    })
    this.loadProList(this.data.TabCur)
  },

  /**
  * 加载更多
  */
  loadProList(index = 0) {
    var pData = this.data;
    pData.pages[index] = {
      page: 0, flag: true
    }
    this.setData({
      pages: pData.pages
    })
    this.getList(index)
  },

  /**
   * 获取列表
   */
  getList(index = 0) {
    var self = this, pData = self.data, pages = pData.pages, url = 'wxapi/days_list', params = {}
    params.start = pages[index].page
    pages[index].page++
    params.month = self.month
    params.order_type = self.order_type
    app.get(url, params).then(res => {
		console.log(res)
      pages[index].totalPage = res.page_num;
      if (res.page_num > pages[index].page) {
        pages[index].flag = false;

      }
      self.setData({
        [`pros[${index}][${pages[index].page - 1}]`]: res.list,
		orders_store_money:res.orders_store_money,
		percent:res.percent,month1:res.month1,month2:res.month2,
        pages: pages,
        orstae:res.orstae
      });
    });
  },

  /**
  * 页面上拉触底事件的处理函数
  */
  onReachBottom() {
    var self = this, pData = this.data;
    if (pData.pages[pData.TabCur].flag) return;
    pData.pages[pData.TabCur].flag = true;
    self.setData({ pages: pData.pages });
    self.getList(pData.TabCur);
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {
    return app.defauleSetShare();
  },

  /**
   * 用户点击右上角分享朋友圈
   */
  onShareTimeline() {
    return app.timeShare()
  }



})