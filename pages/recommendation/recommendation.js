var _defineProperty2 = require("../../@babel/runtime/helpers/defineProperty");

var app = getApp();

Page({
    data: _defineProperty2({
        shopName: "",
        tel: "",
        address: ""
    }, "address", ""),
    onShareAppMessage: function onShareAppMessage() {
        return {};
    },
    onLoad: function onLoad(options) {
        wx.setNavigationBarTitle({
            title: "合作推荐"
        });
    },
    shopNameHandler: function shopNameHandler(event) {
        this.setData({
            shopName: event.detail.value
        });
    },
    telHandler: function telHandler(event) {
        this.setData({
            tel: event.detail.value
        });
    },
    addressHandler: function addressHandler(event) {
        this.setData({
            address: event.detail.value
        });
    },
    descHandler: function descHandler(event) {
        this.setData({
            desc: event.detail.value
        });
    },
    submit: function submit() {
        if (!this.data.shopName) {
            wx.showToast({
                title: "请输入门店名称",
                icon: "error",
                duration: 3e3
            });
            return false;
        }
        if (!this.data.tel) {
            wx.showToast({
                title: "请输入电话",
                icon: "error",
                duration: 3e3
            });
            return false;
        }
        if (!/^1\d{10}$/.test(this.data.tel)) {
            wx.showToast({
                title: "电话错误",
                icon: "error",
                duration: 3e3
            });
            return false;
        }
        if (!this.data.address) {
            wx.showToast({
                title: "请输入门店地址",
                icon: "error",
                duration: 3e3
            });
            return false;
        }
        // if(!this.data.desc){
        //     wx.showToast({
        //         title: '请输入合作奖励',
        //         icon: 'error',
        //         duration: 3000
        //     });
        //     return false;
        // }
        //遮罩
                wx.showToast({
            title: "保存中",
            icon: "loading",
            duration: 1e4
        });
        wx.request({
            url: app.globalData.wpturl + "/plugs/wxmessage/weixin/msgApi.ashx",
            type: "get",
            data: {
                sitenum: app.globalData.sitenum,
                openid: app.globalData.openid,
                formid: "50",
                control_262: this.data.shopName,
                control_263: this.data.tel,
                control_264: this.data.address,
                control_265: this.data.desc
            },
            success: function success(res) {
                wx.hideToast();
                console.log(res);
                if (res.statusCode == 200 && res.data.success == "true") {
                    wx.showModal({
                        title: "提示",
                        content: "提交成功",
                        confirmText: "确认",
                        showCancel: false
                    }).then(function(event) {
                        wx.navigateBack({
                            delta: 2
                        });
                    }).catch(function(item) {
                        console.log(item);
                    });
                }
            },
            fail: function fail() {
                wx.hideToast();
            }
        });
    }
});